<?php

namespace Drupal\swiftmailer_emogrify\Plugin\Mail;

use Drupal\swiftmailer\Plugin\Mail\SwiftMailer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Swift Mailer' plugin to send emails.
 *
 * @Mail(
 *   id = "swiftmailer_emogrify",
 *   label = @Translation("Swift Mailer Emogrify"),
 *   description = @Translation("Swift Mailer Emogrify Plugin.")
 * )
 */
class SwiftMailerEmogrify extends SwiftMailer {

  /**
   * The emogrify service.
   *
   * @var \Drupal\swiftmailer_emogrify\Mail\EmogrifyInterface
   */
  protected $emogrify;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $container->get('swiftmailer.transport'),
      $container->get('config.factory')->get('swiftmailer.message'),
      $container->get('logger.factory')->get('swiftmailer'),
      $container->get('renderer'),
      $container->get('module_handler'),
      $container->get('plugin.manager.mail'),
      $container->get('theme.manager'),
      $container->get('asset.resolver')
    );

    $instance->emogrify = $container->get('swiftmailer_emogrify.emogrify');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function massageMessageBody(array &$message, $is_html) {
    parent::massageMessageBody($message, $is_html);

    if ($is_html) {
      $message['body'] = $this->emogrify->emogrify($message['body']->__toString(), $message['key']);
    }
  }

}
