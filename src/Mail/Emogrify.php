<?php

namespace Drupal\swiftmailer_emogrify\Mail;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Theme\ThemeManagerInterface;
use Pelago\Emogrifier\CssInliner;

class Emogrify implements EmogrifyInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected  $configFactory;

  /**
   * Drupal\Core\Theme\ThemeManagerInterface definition.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected  $themeManager;

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Asset\LibraryDiscoveryInterface definition.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected $themeList;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Emogrify constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   *   The library discovery service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeExtensionList $theme_list
   *   The theme extension list.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ThemeManagerInterface $theme_manager, FileSystemInterface $file_system, LibraryDiscoveryInterface $library_discovery, ModuleHandlerInterface $module_handler, ThemeExtensionList $theme_list, LoggerChannelFactoryInterface $logger) {
    $this->configFactory = $config_factory;
    $this->themeManager = $theme_manager;
    $this->fileSystem = $file_system;
    $this->libraryDiscovery = $library_discovery;
    $this->moduleHandler = $module_handler;
    $this->themeList = $theme_list;
    $this->logger = $logger->get('swiftmailer_emogrify');
  }

  /**
   * {@inheritdoc}
   */
  public function emogrify($html, $mail_key, $theme = NULL) {
    if ($theme === NULL) {
      if ($this->themeManager->hasActiveTheme()) {
        $theme = $this->themeManager->getActiveTheme()->getName();
      }
    else {
        $theme = $this->getDefaultTheme();
      }
    }

    if ($html instanceof Markup) {
      $html = (string) $html;
    }
    if (!is_string($html)) {
      return $html;
    }
    $css = $this->getCssTheme($theme, $mail_key);
    if (empty($css)) {
      return $html;
    }
    try {
      $html_inline_css = CssInliner::fromHtml($html)->inlineCss($css)->renderBodyContent();
    }
    catch (\Exception $e) {
      $this->logger->error('Error occured while inlining css into html: ' . $e->getMessage());
      return $html;
    }
    return Markup::create($html_inline_css);
  }

  /**
   * Gets the default theme.
   *
   * @return string
   *   The default theme.
   */
  protected function getDefaultTheme() {
    $default_theme = $this->configFactory->get('system.theme')->get('default');
    return $default_theme;
  }

  /**
   * Gets the css from default theme.
   *
   * @param string $theme
   *   The theme name.
   * @param string $mail_key
   *   The mail key.
   *
   * @return string
   */
  protected function getCssTheme($theme, $mail_key) {
    $css_files = $this->getCssFiles($theme, $mail_key);
    $this->moduleHandler->alter('emogrify_css_files', $css_files);

    $css = '';
    foreach ($css_files as $css_file) {
      if ($data_file = $this->fileSystem->realpath($css_file)) {
        $css = $css . PHP_EOL . file_get_contents($data_file);
      }
    }

    $this->moduleHandler->alter('emogrify_css', $css);

    return $css;
  }

  /**
   * Returns a list of mail css files.
   *
   * @param string $theme
   *   The theme name.
   * @param string $mail_key
   *   The mail key.
   *
   * @return array
   *   The css file path list.
   */
  protected function getCssFiles($theme, $mail_key) {
    $css_files = [];
    $list = $this->themeList->getList();

    $themes = [];
    $loop = TRUE;
    do {
      if (isset($list[$theme])) {
        $themes[] = $theme;

        if (isset($list[$theme]->base_theme)) {
          $theme = $list[$theme]->base_theme;
        }
        else {
          $loop = FALSE;
        }
      }
      else {
        $loop = FALSE;
      }
    }while($loop);

    $themes = array_reverse($themes);
    foreach ($themes as $theme) {
      $theme_libraries = $this->libraryDiscovery->getLibrariesByExtension($theme);

      foreach (['mail', 'mail_' . $mail_key] as $lib) {
        if (isset($theme_libraries[$lib]['css'])) {
          foreach ($theme_libraries[$lib]['css'] as $css_lib) {
            if (!empty($css_lib['data'])) {
              $css_files[] = $css_lib['data'];
            }
          }
        }
      }
    }
    return $css_files;
  }

}
