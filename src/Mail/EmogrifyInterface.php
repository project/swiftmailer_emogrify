<?php

namespace Drupal\swiftmailer_emogrify\Mail;

interface EmogrifyInterface {

  /**
   * Convert stylesheet rules to inline style attributes.
   *
   * @param string $html
   *   The html.
   * @param string $mail_key
   *   The mail key.
   * @param string $theme
   *   The theme to apply.
   *
   * @return string
   *   The new html string.
   */
  public function emogrify($html, $mail_key, $theme = NULL);
}
