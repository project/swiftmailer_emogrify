<?php

namespace Drupal\Tests\swiftmailer_emogrify\Kernel;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * EmogrifyTest.
 *
 * @group swiftmailer_emogrify
 */
class EmogrifyTest extends EntityKernelTestBase {

  use AssertMailTrait;

  /**
   * The emogrify service.
   *
   * @var \Drupal\swiftmailer_emogrify\Mail\EmogrifyInterface
   */
  protected $emogrify;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'mailsystem',
    'swiftmailer',
    'swiftmailer_emogrify',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    \Drupal::service('theme_installer')->install(['swiftmailer_emogrify_test_theme']);
    $this->installConfig(['mailsystem']);


    // $this->installEntitySchema('profile');

    $this->emogrify = \Drupal::service('swiftmailer_emogrify.emogrify');
    $this->mailManager = $this->container->get('plugin.manager.mail');
  }

  /**
   * Test emogrify.
   */
  public function testEmogrify() {
    // Simple html check.
    $html = '<div class="test-class-1"><div class="test-class-2">Test</div></div>';
    $new_html = $this->emogrify->emogrify($html, 'test', 'swiftmailer_emogrify_test_theme');
    $this->assertNotEquals($html, $new_html);

    // Check via mail send.
    $mailsystem_config = $this->config('mailsystem.settings');
    $mailsystem_config
      ->set('defaults.sender', 'swiftmailer')
      ->set('defaults.formatter', 'swiftmailer_emogrify')
      ->set('theme', 'swiftmailer_emogrify_test_theme')
      ->save();
    $swiftmailer_config = $this->config('swiftmailer.message');
    $swiftmailer_config
      ->set('content_type', 'text/html')
      ->set('text_format', 'filtered_html')
      ->set('generate_plain', FALSE)
      ->set('character_set', 'UTF-8')
      ->save();

    $account = $this->createUser([
      'mail' => 'test@test.de',
    ]);

    $mail = $this->mailManager->mail('user', 'register_admin_created', $account->getEmail(), $account->getPreferredLangcode(), ['account' => $account]);
    $mail_body = $mail['body']->__toString();
    $this->assertNotFalse(strpos($mail_body, 'class="test-class-1" style="padding: 40px;"'));
    $this->assertNotFalse(strpos($mail_body, 'class="test-class-2" style="font-size: 24px;"'));
  }

}
