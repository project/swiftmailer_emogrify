# SwiftMailer Emogrify
This module provides an integration of pelago/emogrifier. It inserts inline styles into HTML from CSS for better sending
emails. Because CSS files and classes are often removed by various email provider, this will ensure that your styles
will stay in your emails, so they will more likely be perceived by the recipient as intended.
The module comes with a mail formatter plugin using the swiftmailer, to replace the mail body before the sending.

# Features
- Send HTML emails with inline css styles to drastically increase the chance that your emails will be perceived as intended
- Low effort configuration: Set the formatter, add your css via library, done
- Can add extra css, based on the specific email being sent

# Usage
- Install this module as usual
- Go to /admin/config/system/mailsystem
- Select the "Swift Mailer Emogrify" plugin as the formatter.¹
- Add a new library to your THEME.libraries.yml using the 'mail' key and add your css file(s):
<pre>mail:
  css:
    component:
      css/mail.css: {}
</pre>
CSS in emails is pretty restricted. There are many things that do not work as you expect on a website like media queries,    
so it is recommended to prepare a CSS file for explicit email use only.
- If needed, you can provide extra CSS for specific emails which need special styling because of their special templates:
<pre>mail_MAIL_KEY:
  css:
    component:
      css/additional_mail.css: {}
</pre>
Example: You're using Drupal's commerce module and want to style the order receipt mail, you would add a library **"mail_order_receipt"**.

¹ Note: the "Swift Mailer Emogrify" plugin can also be selected as a "sender" because a mail plugin provides both
a formatter and sender, but it does exactly the same as the default "Swift Mailer".
