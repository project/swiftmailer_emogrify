<?php

/**
 * @file
 * Hooks provided by the swiftmailer_emogrify module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alters the list of css files, which will be loaded.
 *
 * @param array $css_files
 *   The css files list.
 *
 * @ingroup swiftmailer_emogrify
 */
function hook_emogrify_css_files_alter(array &$css_files) {
  $css_files[] = 'path/to/custom.css';
}

/**
 * May alter the collected css directly.
 *
 * @param array $css
 *   The css.
 *
 * @ingroup swiftmailer_emogrify
 */
function hook_emogrify_css_alter(array &$css) {
  $css .= 'some_extra: css;';
}

/**
 * @} End of "addtogroup hooks".
 */
